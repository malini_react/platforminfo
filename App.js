import React, { Component } from 'react';
import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer } from 'react-navigation';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import LoginComponent from './/src/Component/LoginComponent';
import DashBoardComponent from './/src/Component/DashBoardComponent';
import SettingsComponent from './/src/Component/SettingsComponent';

const Tab = createBottomTabNavigator(
  {
    DashBoard : DashBoardComponent,
    Settings : SettingsComponent
},
{  
  initialRouteName: "DashBoard",
  tabBarOptions : {
    activeBackgroundColor : '#00BCD4',
    activeTintColor : '#ffffff',
    inactiveTintColor : '#00BCD4',
  labelStyle : {
    alignSelf : 'center',
    fontSize : 14,
    marginBottom : 15
  }
}
} 
);


const Stack = createStackNavigator(
  {
    Login: LoginComponent,
    Home : Tab,
  },
  {
    headerMode: 'none',
  }
)

const AllScreen = createAppContainer(Stack)

export default class App extends Component {
  render() {
    return (
        <AllScreen/>
    );
  }
}

/*
Created by Malini 18/05/20
*/
import React, { Component } from 'react';
import { View, Text, StatusBar, Platform, BackHandler} from 'react-native'
import { Header, } from 'native-base';
import DeviceInfo from 'react-native-device-info';
import styles from '../Container/Styles'

export default class DashBoardComponent extends Component {
  constructor(props){
    super(props);
    this.state = {
      DeviceInfo : ''
    }
  }
  
  componentDidMount(){
    console.disableYellowBox = true;
    let model = DeviceInfo.getModel();
    var modelName = model.split(' ').slice(0,2).join(' ');
    if(Platform.OS == 'android' && modelName == "Android SDK"){
      this.setState({DeviceInfo : 'Running on emulator' })
    }
    else if(Platform.OS == 'android'){
      this.setState({DeviceInfo : 'Running on device' })
    }
    else{
      this.setState({DeviceInfo : 'Running on simulator' })
        }
  }

  handleBackPress = () => {
    BackHandler.exitApp();
   return true;
  }

  componentWillMount(){
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
  }

  componentWillUnmount(){
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
  }
 
    render() {
        return (
            <View style={styles.ParentView}>
            <Header style = {styles.HeaderStyle}>
            <Text style = {styles.TextStyle}>{'DashBoard'}</Text>
              </Header>
              <StatusBar hidden={true} />   
               <View style = {styles.BannerView}>
               <Text style = {styles.BannerText}>{this.state.DeviceInfo}</Text>
               </View>
               <View style = {styles.emailView}>
                 <Text style = {styles.ContentText}>{this.props.navigation.state.params.email}</Text>
               </View>
            </View>
        );
    }
}



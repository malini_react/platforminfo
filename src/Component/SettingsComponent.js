/*
Created by Malini 18/05/20
*/
import React, { Component } from 'react';
import { View, Text, StatusBar,  Switch, } from 'react-native'
import {Header} from 'native-base';
import DeviceInfo from 'react-native-device-info';
import styles from '../Container/Styles'

export default class DashBoardComponent extends Component {
    constructor(props){
        super(props);
        this.state = {
            isEnabled : false,
            DeviceInfo : '',
        }
    }
   
    componentDidMount(){
        console.disableYellowBox = true;
    let model = DeviceInfo.getModel();
    var modelName = model.split(' ').slice(0,2).join(' ');
    if(Platform.OS == 'android' && modelName == "Android SDK"){
      this.setState({DeviceInfo : 'Running on emulator' })
    }
    else if(Platform.OS == 'android'){
      this.setState({DeviceInfo : 'Running on device' })
    }
    else{
      this.setState({DeviceInfo : 'Running on simulator' })
        }
    }

    handleBackPress = () => {
        BackHandler.exitApp();
       return true;
      }
    
      componentWillMount(){
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
      }
    
      componentWillUnmount(){
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
      }

    toggleSwitch(){
        this.setState({isEnabled: !this.state.isEnabled});
    }
 
    render() {
        return (
            <View style={styles.ParentView}>
             <Header style = {styles.HeaderStyle}>
            <Text style = {styles.TextStyle}>{'Settings'}</Text>
              </Header>
              <StatusBar hidden={true} />   
              <View style = {styles.BannerView}>
                {this.state.isEnabled ?
               <Text style = {styles.BannerText}>{this.state.DeviceInfo}</Text> : null}
               </View>
               <View style = {styles.emailView}>
               <Switch
        trackColor={{ false: "#767577", true: "#81b0ff" }}
        thumbColor={this.state.isEnabled ? "#f5dd4b" : "#f4f3f4"}
        ios_backgroundColor="#3e3e3e"
        onValueChange={this.toggleSwitch.bind(this)}
        value={this.state.isEnabled}
        style = {{alignSelf : 'center'}}
      />               
      </View>
            </View>
        );
    }
}




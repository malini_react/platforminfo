/*
Created by Malini 18/05/20
*/
import React, { Component } from 'react';
import { View, Text, TouchableOpacity, SafeAreaView, StatusBar, TextInput, Keyboard} from 'react-native'
import Toast from 'react-native-easy-toast';
import styles from '../Container/Styles'

export default class LoginComponent extends Component {
  constructor(props){
    super(props);
    this.state = {
      email : '',
      password : ''
    }
  }

  componentDidMount(){
    console.disableYellowBox = true;
  }

  checkLogin(){
      Keyboard.dismiss();
    const emailReg = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
    if(this.state.email == "" || emailReg.test(this.state.email) == false){
        this.refs.toast.show('Enter a valid email', 1500);
    }
    else if(this.state.password == ""){
        this.refs.toast.show('Enter password', 1500);
    }
    else if (this.state.email == 'admin@gmail.com' && this.state.password == 'asd123'){
      this.props.navigation.navigate('Home', {email: this.state.email})
    }
    else{
        this.refs.toast.show('Authentication failed. User not found', 1500);
    }
  }
  render(){
    return(
      <SafeAreaView style={styles.ParentView} >
      <StatusBar hidden={true} />
      <Toast ref="toast"
            style={styles.ToastStyle}
            position='bottom'
            fadeInDuration={750}
            fadeOutDuration={1000}
            opacity={0.8}
            textStyle={{ color: '#fff' }}
          />
      <View style={styles.ContentView} >
          <TextInput style={styles.TextInput}
              ref={input => { this.textInput1 = input }}
              placeholder="Email"
              onChangeText={(email) => this.setState({ email })}
              value={this.state.email} />
          <TextInput style={styles.TextInput}
              ref={input => { this.textInput2 = input }}
              placeholder="Password"
              onChangeText={(password) => this.setState({ password })}
              value={this.state.password}
              secureTextEntry />
          <TouchableOpacity onPress={() => this.checkLogin()}
              style={styles.ButtonStyle}>
              <Text style={styles.TextStyle}>Login</Text>
          </TouchableOpacity>
      </View>
  </SafeAreaView>

    )
  }
}


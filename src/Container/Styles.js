/*
Created by Malini 19/05/20
*/
import { StyleSheet, Dimensions} from 'react-native'

const BannerWidth = Dimensions.get('window').width;
const BannerHeight = Dimensions.get('window').height;

export default StyleSheet.create({
    ParentView : {
        flex : 1,
        backgroundColor: '#fff' , 
        justifyContent : 'center'
    },
    ToastStyle : {
        backgroundColor: '#000' 
    },
    ContentView : { 
        justifyContent: 'center', 
        alignItems: 'center' 
    },
    TextInput : {
         backgroundColor: '#fff', 
         height: 50, 
         width: '90%', 
         marginTop: 10, 
         borderColor : 'grey', 
         borderWidth : 1 
    },
    ButtonStyle : {
        alignItems: 'center', 
        justifyContent: 'center', 
        width: '90%',
        height: 40, 
        backgroundColor: '#00BCD4', 
        marginTop: 10
    },
    TextStyle : { 
        color: '#fff', 
        fontWeight: 'bold', 
        fontSize: 18 
    },
    HeaderStyle : {
        height : 60, 
        backgroundColor : '#00BCD4', 
        justifyContent : 'center', 
        alignItems: 'center'
    },
    BannerText : {
        alignSelf : 'center', 
        color : '#fff', 
        fontSize : 18
    },
    ContentText : {
        alignSelf : 'center', 
        color : '#000', 
        fontSize : 18
    },
    emailView: {
        flex : 1,
        justifyContent : 'center',
    },
    BannerView : {
        width : BannerWidth, 
        height : BannerHeight/3, 
        backgroundColor : 'orange', 
        justifyContent : 'center'
    }
})